import sys

import compiler.cmm as cmm

""" Test if cmm lexer is working.  It does the same work as lab_1
"""

def get_code_attr(token):
  """ Given token, return tuple containing its type code and attribute.
      The first item will be type code and the second item will be
      attribute.
      Note that the second item will be None if that token type does not
      have attribute value.
  """
  codes = {
    "&&"        : 256,
    "||"        : 257,
    "<="        : 258,
    ">="        : 259,
    "=="        : 260,
    "!="        : 261,
    "int"       : 262,
    "if"        : 263,
    "else"      : 264,
    "while"     : 265,
    "break"     : 266,
    "continue"  : 267,
    "scan"      : 268,
    "print"     : 269,
    "println"   : 270,
  }
  if isinstance(token.value, tuple):
    token.value = token.value[1]
  if isinstance(token.value, str):
    if token.type=="ID":
      return (271, token.value)
    elif token.type=="STR":
      return (273, token.value)
    else:
      if len(token.value)==1:
        return (ord(token.value), None)
      else:
        if token.value in codes:
          return (codes[token.value], None)
        else:
          raise ValueError
  elif isinstance(token.value, int):
    return (272, "{:#010x}".format(token.value))
  else:
    raise ValueError

def __main__():
  with open(sys.argv[1], 'r') as input_file:
    input_content = ''.join(input_file)
  cmm.lexer.input(input_content)
  with open(sys.argv[2], 'w') as output_file:
    for token in cmm.lexer:
      pair = get_code_attr(token)
      if pair[1] is None:
        output_file.write('{}\n'.format(pair[0]))
      else:
        output_file.write('{}\t{}\n'.format(pair[0], pair[1]))

if __name__=='__main__':
  __main__()

